package com.atlassian.marketplace.controllers

import com.atlassian.marketplace.models.Account
import com.atlassian.marketplace.services.AccountService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/accounts")
class AccountController(@Autowired private val accountService: AccountService) {
    @GetMapping("/{id}")
    fun getAccount(@PathVariable("id") id: Long) = accountService.getAccount(id)

    @PostMapping
    fun postAccount(@RequestBody account: Account) = ResponseEntity(accountService.addAccount(account), HttpStatus.CREATED)

    @PutMapping("/{id}")
    fun putAccount(@PathVariable("id") id: Long,
                   @RequestBody account: Account) = accountService.updateAccount(id, account)

    @GetMapping("/{id}/contacts")
    fun getAccountContacts(@PathVariable("id") id: Long) = accountService.getAccountContacts(id)
}