package com.atlassian.marketplace.controllers

import com.atlassian.marketplace.models.Contact
import com.atlassian.marketplace.services.ContactService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/contacts")
class ContactController(@Autowired private val contactService: ContactService) {
    @GetMapping("/{id}")
    fun getContact(@PathVariable("id") id: Long) = contactService.getContact(id)

    @PostMapping
    fun postContact(@RequestBody contact: Contact) = ResponseEntity(contactService.addContact(contact), HttpStatus.CREATED)

    @PutMapping("/{id}")
    fun putContact(@PathVariable("id") id: Long,
                   @RequestBody contact: Contact) = contactService.updateContact(id, contact)
}