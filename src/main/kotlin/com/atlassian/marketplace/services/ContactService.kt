package com.atlassian.marketplace.services

import com.atlassian.marketplace.models.Contact
import com.atlassian.marketplace.repositories.ContactRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class ContactService(val contactRepository: ContactRepository) {
    fun getContact(id: Long): Contact = contactRepository.findById(id).orElseThrow {
        ResponseStatusException(HttpStatus.NOT_FOUND, "Contact ${id} not found")
    }

    fun addContact(contact: Contact): Contact = contactRepository.save(contact)

    fun updateContact(id: Long, updatedContact: Contact): Contact {
        // TODO: error handling
        val contact = contactRepository.findById(id).orElse(return contactRepository.save(updatedContact))

        // TODO: there has to be a better way to do this
        contact.apply {
            name = updatedContact.name
            emailAddress = updatedContact.emailAddress
            addressLine1 = updatedContact.addressLine1
            addressLine2 = updatedContact.addressLine2
            city = updatedContact.city
            state = updatedContact.state
            postalCode = updatedContact.postalCode
            country = updatedContact.country
            accountId = updatedContact.accountId
        }
        return contactRepository.save(contact)
    }

}