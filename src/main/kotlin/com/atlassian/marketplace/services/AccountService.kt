package com.atlassian.marketplace.services

import com.atlassian.marketplace.models.Account
import com.atlassian.marketplace.models.Contact
import com.atlassian.marketplace.repositories.AccountRepository
import com.atlassian.marketplace.repositories.ContactRepository
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class AccountService(val accountRepository: AccountRepository, val contactRepository: ContactRepository) {
    fun getAccount(id: Long): Account = accountRepository.findById(id).orElseThrow {
        ResponseStatusException(HttpStatus.NOT_FOUND, "Account ${id} not found")
    }

    fun addAccount(account: Account): Account = accountRepository.save(account)

    fun updateAccount(id: Long, updatedAccount: Account): Account {
        // TODO: error handling
        val account = accountRepository.findById(id).orElse(return accountRepository.save(updatedAccount))

        // TODO: there has to be a better way to do this
        account.apply {
            companyName = updatedAccount.companyName
            addressLine1 = updatedAccount.addressLine1
            addressLine2 = updatedAccount.addressLine2
            city = updatedAccount.city
            state = updatedAccount.state
            postalCode = updatedAccount.postalCode
            country = updatedAccount.country
        }
        return accountRepository.save(account)
    }

    fun getAccountContacts(id: Long): List<Contact> = contactRepository.findByAccountIdOrderByName(id)
}

