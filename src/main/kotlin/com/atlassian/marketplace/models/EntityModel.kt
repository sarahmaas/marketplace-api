package com.atlassian.marketplace.models

import javax.persistence.*


@Entity
@Table(name = "contact")
data class Contact(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        val id: Long = 0,

        @Column(name = "name")
        val name: String = "",

        @Column(name = "email_address", unique = true)
        val emailAddress: String = "",

        @Column(name = "address_line_1")
        val addressLine1: String = "",

        @Column(name = "address_line_2")
        val addressLine2: String = "",

        @Column(name = "city")
        val city: String = "",

        @Column(name = "state")
        val state: String = "",

        @Column(name = "postal_code")
        val postalCode: String = "",

        @Column(name = "country")
        val country: String = "",

        @Column(name = "account_id", nullable = true)
        val accountId: Long? = null,
)

@Entity
@Table(name = "account")
data class Account(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        val id: Long = 0,

        @Column(name = "company_name", unique = true)
        val companyName: String = "",

        @Column(name = "address_line_1")
        val addressLine1: String = "",

        @Column(name = "address_line_2")
        val addressLine2: String = "",

        @Column(name = "city")
        val city: String = "",

        @Column(name = "state")
        val state: String = "",

        @Column(name = "postal_code")
        val postalCode: String = "",

        @Column(name = "country")
        val country: String = "",
)