package com.atlassian.marketplace.repositories

import com.atlassian.marketplace.models.Contact
import org.springframework.stereotype.Repository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param

@Repository
interface ContactRepository: JpaRepository<Contact, Long> {
    fun findByAccountIdOrderByName(@Param("accountId") accountId: Long): List<Contact>
}