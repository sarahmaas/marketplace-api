package com.atlassian.marketplace.repositories

import com.atlassian.marketplace.models.Account
import org.springframework.stereotype.Repository
import org.springframework.data.jpa.repository.JpaRepository

@Repository
interface AccountRepository: JpaRepository<Account, Long>  {
}