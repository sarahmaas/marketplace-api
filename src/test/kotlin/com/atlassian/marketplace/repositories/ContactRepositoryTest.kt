package com.atlassian.marketplace.repositories

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@DataJpaTest
class ContactRepositoryTest {
    @Autowired
    lateinit var contactRepository: ContactRepository

    // TODO: seed db?
    @Test
    fun testFindByAccountIdOrderByName() {
        var contacts = contactRepository.findByAccountIdOrderByName(1)
        assertNotNull(contacts)
        assertEquals(0, contacts.size)
    }
}