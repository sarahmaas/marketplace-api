package com.atlassian.marketplace.controllers

import com.atlassian.marketplace.models.Contact
import com.atlassian.marketplace.repositories.ContactRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.BDDMockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ContactControllerTest {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @MockBean
    private lateinit var mockContactRepository: ContactRepository

    @Test
    fun testGetContact() {
        val contact = Contact(id = 1, name = "Chester")
        given(mockContactRepository.findById(anyLong())).willReturn(Optional.of(contact))

        val response = testRestTemplate.getForEntity("/contacts/1", Contact::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals("Chester", response.body?.name)
    }

    @Test
    fun testGetContactNotFound() {
        given(mockContactRepository.findById(anyLong())).willReturn(Optional.empty())

        val response = testRestTemplate.getForEntity("/contacts/5", Contact::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.NOT_FOUND, response.statusCode)
    }

    @Test
    fun testPostContact() {
        val contact = Contact(name = "Chester")
        given(mockContactRepository.save(any(Contact::class.java))).willReturn(contact)

        val response = testRestTemplate.postForEntity("/contacts", contact, Contact::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.CREATED, response.statusCode)
        assertEquals("Chester", response.body?.name)
    }

    @Test
    fun testPutContact() {
        val contact = Contact(id = 1, name = "Chester")
        given(mockContactRepository.findById(anyLong())).willReturn(Optional.of(contact))
        given(mockContactRepository.save(any(Contact::class.java))).willReturn(contact)

        val response = testRestTemplate.exchange("/contacts/1", HttpMethod.PUT, HttpEntity(contact), Contact::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(1, response.body?.id)
        assertEquals("Chester", response.body?.name)
    }

    // TODO: further put tests, like bad request
}