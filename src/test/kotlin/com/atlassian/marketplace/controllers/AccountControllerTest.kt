package com.atlassian.marketplace.controllers

import com.atlassian.marketplace.models.Account
import com.atlassian.marketplace.models.Contact
import com.atlassian.marketplace.repositories.AccountRepository
import com.atlassian.marketplace.repositories.ContactRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ExtendWith(SpringExtension::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountControllerTest {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @MockBean
    private lateinit var mockAccountRepository: AccountRepository

    @MockBean
    private lateinit var mockContactRepository: ContactRepository

    @Test
    fun testGetAccount() {
        val account = Account(id = 1, companyName = "Chester's Cheeseballs")
        given(mockAccountRepository.findById(anyLong())).willReturn(Optional.of(account))

        val response = testRestTemplate.getForEntity("/accounts/1", Account::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals("Chester's Cheeseballs", response.body?.companyName)
    }

    @Test
    fun testGetAccountNotFound() {
        given(mockAccountRepository.findById(anyLong())).willReturn(Optional.empty())

        val response = testRestTemplate.getForEntity("/accounts/10", Account::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.NOT_FOUND, response.statusCode)
    }

    @Test
    fun testPostAccount() {
        val account = Account(companyName = "Chester's Cheeseballs")
        given(mockAccountRepository.save(any(Account::class.java))).willReturn(account)

        val response = testRestTemplate.postForEntity("/accounts", account, Account::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.CREATED, response.statusCode)
        assertEquals("Chester's Cheeseballs", response.body?.companyName)
    }

    @Test
    fun testPutAccount() {
        val account = Account(id = 1, companyName = "Chester's Cheeseballs")
        given(mockAccountRepository.findById(anyLong())).willReturn(Optional.of(account))
        given(mockAccountRepository.save(any(Account::class.java))).willReturn(account)

        val response = testRestTemplate.exchange("/accounts/1", HttpMethod.PUT, HttpEntity(account), Account::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(1, response.body?.id)
        assertEquals("Chester's Cheeseballs", response.body?.companyName)
    }

    // TODO: further put tests, like bad request

    @Test
    fun testGetAccountContacts() {
        val account = Account(id = 1, companyName = "Chester's Cheeseballs")
        val contactList = listOf(
                Contact(id = 1, name = "Chester", accountId = account.id),
                Contact(id = 2, name = "Rue", accountId = account.id)
        )
        given(mockContactRepository.findByAccountIdOrderByName(anyLong())).willReturn(contactList)

        val response = testRestTemplate.getForEntity("/accounts/1/contacts", Array<Contact>::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(2, response.body?.size)
    }

    @Test
    fun testGetAccountContactsEmpty() {
        given(mockContactRepository.findByAccountIdOrderByName(anyLong())).willReturn(emptyList())

        val response = testRestTemplate.getForEntity("/accounts/1/contacts", Array<Contact>::class.java)

        assertNotNull(response)
        assertEquals(HttpStatus.OK, response.statusCode)
        assertEquals(0, response.body?.size)

    }
}