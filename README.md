# Marketplace API

## Dependencies:
* Java 11
* make (optional) - commands to run without can be found in the Makefile
## Testing:
Tests can be launched from the Makefile:
```bash
make test
```
## Running the application locally:
Start with:
```bash
make run
```
After running the application, these UIs are available:

* [Swagger](http://localhost:8080/swagger-ui/index.html)
* [H2 Console](http://localhost:8080/h2-console)


## Notes:

**With more time, I would:**

- add Dockerfile
- figure out what was wrong with OneToMany/ManyToOne - nesting issues
- better validation on object models
- create and update timestamps on data objects
- more robust tests
    - more extensive object definitions
    - data jpa tests, especially with mock db data
- better error handling
- review database uniqueness constraints


**In a production setting I would also expect to enable:**

- a separate DB
- role based access
- delete endpoints, depending on retention policy
- mapping to a DTO for response body
- logging
- dockerized deployment

